/** 
 * @name: EasyCookie
 * @author: Frederick M. Rogers
 * @description: Simple constructor function for getting and setting cookies.
 * @version: 1.0.0
 * 
 */


function EasyCookie(name, value, expires) {    
   
    this.name = name;           // Unique cookie key identifier 
    this.value = value;         // Specific cookie value
    this.expires = expires;     // Number of days until cookies expires      

}

EasyCookie.prototype = {

    // Set date value for expiration
    setDate: function() {
        // If the *optional* argument for EXPIRES exists
        var date = new Date();
        if ( (this.expires !== undefined) && (!isNaN(this.expires)) ) { 
            date.setTime(date.getTime() + (this.expires * 24 * 60 * 60 * 1000));
            return "expires=" + date.toGMTString() + ";";           
        } else {        
            date.setTime(date.getTime() + (30 * 24 * 60 * 60 * 1000));     
            return "expires=" + date.toGMTString() + ";"; 
        }
    },

    // setCookie - Sets a cookie with values
    setCookie: function() {

        this.cookie;

        // If the *required* arguments for NAME and VALUE exist  
        if ( this.name !== undefined && this.value !== undefined )  {            
            this.cookie = this.name + "=" + this.value + ";"; 
        } else { 
            console.log("EasyCookie: WARNING - Required parameter(s) for NAME and/or VALUE appear to be missing.");           
        }       
        
        // Set the expiration
        this.cookie += this.setDate();

        // Set default path for cookie
        this.cookie += "path=/;";

        // Finally set the cookie
        document.cookie = this.cookie;       
    },

    // getCookie - Return the key value pairs of the cookie
    getCookie: function() {

        // Set initial value to false
        this.cookieIsSet = false;
        
        // Split the cookie string into an array by semicolon
        var splitCookie = document.cookie.split(";");
        
        // Loop over each index of the array
        for(var i = 0; i < splitCookie.length; i++) {

            // Split each of the index elements into seperate arrays using equals sign
            var splitCookieAgain = splitCookie[i].split("="); 
            
            // If the first index (zero) of the new array match NAME property
            if (splitCookieAgain[0].replace(/\s+/g, '') == this.name) {
                this.cookieIsSet = true;
            }                     
        }

        // Return a boolean
        return this.cookieIsSet;        
    },

    // deleteCookie - Delete the previously set cookie
    deleteCookie: function() {

        // Check if cookie is set
        if (this.getCookie()) {

            // Set the numbers of day til expiration
            this.expires = -1;
            this.setCookie();
        }
        
    }

}

